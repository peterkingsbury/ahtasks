exports.task = {
  name:          'task',
  description:   'task',
  frequency:     0,
  queue:         'default',
  plugins:       [],
  pluginOptions: {},

  run: function(api, params, next){
    api.tasks.enqueue('task', {}, 'default', function(err, toRun){
      if (err) {
        next(err);
      }
      next();
    });
  }
};
