module.exports = {
  loadPriority:  1000,
  startPriority: 1000,
  stopPriority:  1000,
  initialize: function(api, next){
    api.main = {};

    next();
  },
  start: function(api, next){
    api.tasks.enqueue('task', {}, 'default', function(){
      next();
    });
  },
  stop: function(api, next){
    next();
  }
};
